const MongoClient = require('mongodb').MongoClient;
// eslint-disable-next-line
const format = require('util').format;

MongoClient.connect('mongodb://127.0.0.1:27017/test', (err, db) => {
  if (err) {
    throw err;
  } else {
    // eslint-disable-next-line
    console.log('successfully connected to the database');
  }
  db.close();
});
