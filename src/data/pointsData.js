/* eslint-disable */


export default [
  {
    "trail_id": 1,
    point_id: 0,
    name: 'Tartu Ülikooli peahoone',
    description: 'Üks silmapaistvamaid klassitsistliku arhitektuuri näiteid Eestis.',
    lon: '26.71986590000006',
    lat: '58.3810843',
  },
  {
    "trail_id": 1,
    point_id: 1,
    name: 'Arvutiteaduste instituut',
    description: 'Arvutiteaduse instituut on üks Tartu Ülikooli rahvusvahelisima ja kiiremini kasvava koosseisuga instituut.',
    lon: '26.71467329999996',
    lat: '58.37824850000001',
  },
  {
    "trail_id": 2,
    point_id: 2,
    name: 'Tartu Ülikooli Pärnu Kolledž',
    description: 'Tartu ülikooli Pärnu kolledž on Eestis teenuste juhtimise alase hariduse lipulaev juba mitu kümnendit.',
    lon: '24.488988500000005',
    lat: '58.3850177',
  },
  {
    "trail_id": 2,
    point_id: 3,
    name: 'Tartu Ülikooli Narva Kolledž',
    description: 'Tartu ülikooli Narva kolledž pakub TÜ kvaliteedimärgiga kõrgharidust, hoiab ja arendab akadeemilisi traditsioone Kirde-Eestis ning on Eesti ühiskonna integratsiooni edendajaks.',
    lon: '28.199370799999997',
    lat: '59.37910189999999',
  },
  {
    "trail_id": 3,
    point_id: 4,
    name: 'Hanna koduke',
    description: 'Kodu',
    lon: '26.737264',
    lat: '58.362701',
    },
    {
    "trail_id": 3,
    point_id: 5,
    name: 'Kerttu koduke',
    description: 'Kodu',
    lon: '26.72353910000004',
    lat: '58.3557505',
    }
]
