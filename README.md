# SadaRada https://tarkvaraprojekt.herokuapp.com/#/
# GitHub https://github.com/ageroosi/rada100

Mobiilpositsioneerimisel põhinev veebirakendus pärimuskogude tutvustamiseks.


1. iteratsioon:

    Avaleht: https://gitlab.com/htagen/sadarada/wikis/avaleht

    Projektiplaan: https://gitlab.com/htagen/sadarada/wikis/Projektiplaan

    Projekti visioon: https://gitlab.com/htagen/sadarada/wikis/Projekti-visioon

    Funktsionaalsed nõuded: https://gitlab.com/htagen/sadarada/wikis/Funktsionaalsed-n%C3%B5uded

    Mittefunktsionaalsed nõuded: https://gitlab.com/htagen/sadarada/wikis/Mittefunktsionaalsed-n%C3%B5uded

    Kasutuslood: https://gitlab.com/htagen/sadarada/wikis/kasutuslood

    Kliendiga kohtumised: https://gitlab.com/htagen/sadarada/wikis/kliendiga-kohtumised

2. iteratsioon:

    Release note: https://gitlab.com/htagen/sadarada/wikis/Teine-iteratsioon

    Kasutuslood (implementeeritud UC10) + prototüüp: https://gitlab.com/htagen/sadarada/wikis/kasutuslood
    
    Projektiplaan, kus on täiendatud projekti skoop: https://docs.google.com/document/d/1CMCfT1kQavTR4yW40JiXxYIJ7axHZcrV18RX5VrNNZo/edit?usp=sharing
    
    Projekti skoop eraldi: https://drive.google.com/file/d/1P3MzJSfFTO46beQySeDcv-c6Dbb9BmIX/view?usp=sharing

3. iteratsioon:
    
    Release note: https://gitlab.com/htagen/sadarada/wikis/Kolmas-iteratsioon:-release-notes
    
    Kasutuslood + prototüüp: https://gitlab.com/htagen/sadarada/wikis/kasutuslood

    Projekti skoop eraldi: https://drive.google.com/file/d/1P3MzJSfFTO46beQySeDcv-c6Dbb9BmIX/view?usp=sharing
    NB!!! TEGEMIST ON EELMISE ITERATSIOONI SKOOBIGA, kuna GitLab võimaldab issuesid eksportida ainult upgrade-ides. Enne saime teha trial versioonina seda, aga see lõppes ära ja enam ei saa eksportida.
    
Peer review:
    https://docs.google.com/document/d/1gP_x6PVq6pznI7UWji0J5fQ-WATuD9rCP8gt79LTv2w/edit?usp=sharing
    
4. iteratsioon:
    
    Release note: https://gitlab.com/htagen/sadarada/wikis/Neljas-iteratsioon:-release-notes

    Peer review tagasiside: https://gitlab.com/htagen/sadarada/wikis/Peer-review-anal%C3%BC%C3%BCs
    
    Internal acceptance testing: https://docs.google.com/document/d/1-sHKDsRtQ2-U1YzbGAptJaEDweUf0MsVnegX-NM2YQI/edit?usp=sharing
    
    Jõudlustest (testda NFR 4.): https://gitlab.com/htagen/sadarada/wikis/j%C3%B5udlustest
    
    Rakenduse kasutajatega testimine (testda NFR 2.): https://gitlab.com/htagen/sadarada/wikis/L%C3%B5ppkasutajatega-testimised
    
    Kliendiga kohtumised: https://gitlab.com/htagen/sadarada/wikis/kliendiga-kohtumised

Final report: https://gitlab.com/htagen/sadarada/wikis/Final-report
    
    
